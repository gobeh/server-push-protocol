package id.ac.uinjkt.serverpushprotocol.entity;

import id.ac.uinjkt.serverpushprotocol.dto.ParamDto;
import id.ac.uinjkt.serverpushprotocol.services.CommonService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@Controller
@Slf4j
@RequestMapping("/iclock")
public class IClockController {

    @Autowired
    private CommonService commonService;

    //handshake
    @GetMapping(value = "/cdata")
    @ResponseBody
    public String getHandshake(@RequestParam(value = "SN", required = false) String sn,
                               @RequestParam(value = "options", required = false) String options,
                               @RequestParam(value = "pushver", required = false) String pushver,
                               @RequestParam(value = "language", required = false) Integer language, HttpServletRequest request) throws IOException {

        String ipAddress = request.getRemoteAddr();
        log.info("ip address : {}", ipAddress);

        log.info("Request URI : {}, Request URL : {} ", request.getRequestURI(), request.getRequestURL());
        ParamDto paramDto = ParamDto.builder()
                .SN(sn)
                .options(options)
                .pushver(pushver)
                .language(language)
                .build();
        log.info("ParamDto : {}", paramDto);

        String body = commonService.getBody(request);
        log.info("Body : {}", body);
        return "OK";
    }

    //ambil perintah
    @GetMapping(value = "/getrequest")
    @ResponseBody
    public String getCommand(@RequestParam(value = "SN", required = false) String sn,
                             @RequestParam Map<String, String> allRequestParams,
                             HttpServletRequest request) throws IOException {

        String ipAddress = request.getRemoteAddr();
        log.info("ip address : {}", ipAddress);

        log.info("Request URI : {}, Request URL : {} ", request.getRequestURI(), request.getRequestURL());
        ParamDto paramDto = ParamDto.builder()
                .SN(sn)
                .build();
        log.info("ParamDto : {}", paramDto);

        log.info("all params: {}", commonService.convertWithIteration(allRequestParams));

        String body = commonService.getBody(request);
        log.info("Body : {}", body);
        return "OK";
    }

    //post jawaban
    @PostMapping(value = "/devicemd")
    @ResponseBody
    public String postCommand(@RequestParam(value = "SN", required = false) String sn,
                              HttpServletRequest request) throws IOException {

        String ipAddress = request.getRemoteAddr();
        log.info("ip address : {}", ipAddress);

        log.info("Request URI : {}, Request URL : {} ", request.getRequestURI(), request.getRequestURL());
        ParamDto paramDto = ParamDto.builder()
                .SN(sn)
                .build();
        log.info("ParamDto : {}", paramDto);

        String body = commonService.getBody(request);
        log.info("Body : {}", body);

        return "OK";
    }

    //post data
    @PostMapping(value = "/cdata")
    @ResponseBody
    public String postData(@RequestParam(value = "SN", required = false) String sn,
                           @RequestParam(value = "Stamp", required = false) Integer stamp,
                           HttpServletRequest request) throws IOException {

        String ipAddress = request.getRemoteAddr();
        log.info("ip address : {}", ipAddress);

        log.info("Request URI : {}, Request URL : {} ", request.getRequestURI(), request.getRequestURL());
        ParamDto paramDto = ParamDto.builder()
                .SN(sn)
                .stamp(stamp)
                .build();
        log.info("ParamDto : {}", paramDto);
        String body = commonService.getBody(request);
        log.info("Body : {}", body);
        return "OK";
    }
}

