package id.ac.uinjkt.serverpushprotocol.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParamDto {
    private String SN;
    private String options;
    private String pushver;
    private Integer language;
    private Integer stamp;
}
