package id.ac.uinjkt.serverpushprotocol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerPushProtocolApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerPushProtocolApplication.class, args);
    }

}
